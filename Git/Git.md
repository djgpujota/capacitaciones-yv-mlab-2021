<!-- Navbar -->
# Git
---
[Que es git?](#Que-es-git?)

[Comandos de git en consola](#Comandos-de-git-en-consola)

[Clientes gits](#Clientes-gits)

[Clonacion de proyecto por consola y por cliente](#Clonacion-de-proyecto-por-consola-y-por-cliente)

[Commits por consola y por cliente kraken o smart](#Commits-por-consola-y-por-cliente-kraken-o-smart)

[Ramas desde kraken](#Ramas-desde-kraken)

[Merge](#Merge)


[Regresar](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/blob/master/README.md)
___



## Que es git?
Git es un software para el control de versiones.
Esta diseñado para almacenar la versión de software que va modificando el desarrollador y poder acceder a ellas con mayor facilidad.



 
## Comandos-de-git-en-consola
* git init
    * Sirve para crear el repositorio en la carpeta seleccionada

        ![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20init.png)
* git status
    * Sirve para ver los estatus de los archivos en el repositorio

        ![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20status.png)
* git add .
    * agrega los archivos localmente en el repositorio

        ![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20add.png)

* git push
    * Sube los archivos a la nube

        ![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20push.png)
* git pull 
    * Descarga los archivos de la nuve

        ![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20pull.png)

 
 
 
 
 ## Clientes gits
 Los clientes git son interfazes graficas para manejar un repositorio git

![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/cliente%20git.png)

![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/cliente%20git%202.png)


 ## Clonacion de proyecto por consola y por cliente
 * Para clonar un repositorio se de ejecutar el comando git clone "url del repositorio"

    ![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20clone.png)

 * Para clonar un repositorio desde gitkraken se de ir a 

    * file

    * clone repo

    ![image](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/clonar%20por%20kraken%201.png)

    
 * Despues se depe ingresar el directorio a donde se dea clonar y copiar la url del repositorio que se desea clonar

    ![image](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/clonar%20por%20kraken%202.png)

 ## Commits por consola y por cliente kraken o smart
 * git commit -m "mensaje"
    * guarda los archivos modificados en nuestro repositorio

    ![image](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20commit.png)

* Para hacer un commit por kraken se deben agregar los archivos a staged y despues agregar el mensaje en el commit y guardar los commit changes

    ![image](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/commit%20por%20cliente.png)


## Ramas desde kraken
Para crear una rama desde kraken se debe ir a Branch y despues se nombra a la rama

![image](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/rama%20kraken.png)


## Merge
Permite integrar las subramas a una sola rama 

![image](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/merge%20kraken.png)
