# GitLab
[Gitlab imformacion que es como accedo](#Gitlab-imformacion-que-es-como-accedo)

[Crear repositorios ](#Crear-repositorios)

[Crear grupos](#Crear-grupos)

[Crear subgrupos](#Crear-subgrupos)

[Crear issues](#Crear-issues)

[Crear labels ](#Crear-labels)

[Permisos cuales hay para que sirven](#Permisos-cuales-hay-para-que-sirven)

[Agregar mienmbros](#Agregar-mienmbros)

[Crear boards y manejo de boards](#Crear-boards-y-manejo-de-boards)

[Commits desde ditlab y crear ramas desde gitlab](#Commits-desde-ditlab-y-crear-ramas-desde-gitlab)

[Regresar](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/blob/master/README.md)



## Gitlab imformacion que es como accedo
Gitlab es  un gestor de repositorios en la nuve para acceder se debe registrarse en gitlab.com.

![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab%20registro.png)

## Crear repositorios 
Para crear un repositorio dentro de gitlab se debe dar click en new proyect.

![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab%201.png)

Despues en create blank project y le damos un nombre al proyecto seleccionamos si quiere ser privado o publico y le damos check al initialize with readme, por ultimo le damos a create proyect.

![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab%202.png)

## Crear grupos 
Para crear grupos en gitlab se debe ir a 
* groups
* create group

![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/grupos%201.png)

Despues se debe agregar un nombre al grupo, se debe especificar si es publico o pribado y se deben agregar el email de los mienbros que se desee que esten en el grupo

 ![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/grupos%202.png)

## Crear subgrupos 
Para crear un subgrupo se debe ir al grupo creado y despues a new subgroup

 ![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/subgrupo%201.png)

Ingresamos el nombre del subgrupo y los mienbros

![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/subgrupo%202.png)

## Crear issues
Para crear issues se debe ir a issues y new issues

 ![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/issues%20.png)

 Despues le damos el nombre al issues , le agreegamos una descripcion y le damos submit issues para enviarlo

 ![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/issues%200.png)


## Crear labels 
Para crear etiquetas se debe ir a labels y create new label

![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/label1.png)
![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/label2.png)

## Permisos cuales hay para que sirven
![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/roles%20gitlab.png)

* Guest
    * Crear un nuevo problema
    * Puede dejar comentarios
    * Capaz de escribir en el muro del proyecto
* Reporter
    * Crear un nuevo problema
    * Puede dejar comentarios
    * Capaz de escribir en el muro del proyecto
    * Capaz de extraer el codigo del proyecto
    * Puede descargar el proyecto
    * Capaz de escribir fragmentos de codigo
* Developer
    * Crear un nuevo problema
    * Puede dejar comentarios
    * Capaz de escribir en el muro del proyecto
    * Capaz de extraer el codigo del proyecto
    * Puede descargar el proyecto
    * Capaz de escribir fragmentos de codigo
    * Cree un nueva solicitud de combinacion
    * Crear una nueva rama
    * Insertar y eliminar ramas desprotegidas
    * Incluir etiquetas	
    * Puede crer, modificar, eliminar hitos del proyecto
    * Puede crear o actualizar el estado de validacion
    * Escribe una wiki
    * Crear nuevos entornos
    * Cancelar y reintentar trabajos
    * Actualiza y elimina la imagen registro

* Maintainer
    * Crear un nuevo problema
    * Puede dejar comentarios
    * Capaz de escribir en el muro del proyecto
    * Capaz de extraer el codigo del proyecto
    * Puede descargar el proyecto
    * Capaz de escribir fragmentos de codigo
    * Cree un nueva solicitud de combinacion
    * Crear una nueva rama
    * Insertar y eliminar ramas desprotegidas
    * Incluir etiquetas	
    * Puede crer, modificar, eliminar hitos del proyecto
    * Puede crear o actualizar el estado de validacion
    * Escribe una wiki
    * Crear nuevos entornos
    * Cancelar y reintentar trabajos
    * Actualiza y elimina la imagen registro
    * Puede agregar nuevos miembros al equipo
    * Insertar y eliminar ramasprotegido
    * Puede editar proyecto
    * Puede administrar corredores, desencadenadores de tareas y variables
    * Agregar claves de implementacion al proyecto
    * Capaz de administrar clusteres
    * Configurar enlaces de proyectos
    * Puede habilitar / deshabilitar la proteccion de rama
    * Capaz de reescribir o eliminar etiquetas Git



## Agregar mienmbros 
Para agregar mienbros se debe ir a menber y se debe agregar el mienbro por email o nombre de usuario

![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/mienbros.png)

## Crear boards y manejo de boards
Para crear una board se debe ir a boards y create new board

![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/boards1.png)

En edit board se puede editar las boards y dividirlas por etiquetas

![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/boards%202.png)

## Commits desde gitlab y crear ramas desde gitlab

Para hacer un commit por gitlab se debe añadir o actulizar informacion del repositorio 

![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/commit%20gitlab.png)

Para crear ramas desde gitlab se debe ir a new branch y despues darle nombre a la rama y create branch.

![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/ramas%20gitlab%201.png)

![imagen](https://gitlab.com/djgpujota/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/ramas%20gitlab%202.png)